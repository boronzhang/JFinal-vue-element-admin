package com.sandu.common.model;

import com.sandu.common.model.base.BaseFeedback;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class Feedback extends BaseFeedback<Feedback> {
	public static final Feedback dao = new Feedback().dao();
}
